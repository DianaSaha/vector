#include "Vector.h"
#include <stdexcept>
#include <cmath>

Vector::~Vector()
{
    _data = nullptr;
    _size = 0;
    _capacity = 0;
    _multiplicativeCoef = 0;
}

Vector::Vector(const Value* rawArray, const size_t size, float coef)
{
    _multiplicativeCoef = coef;
    _size = size;
    _capacity = size;
    _data = new Value[_capacity];
    
    for(size_t i = 0; i < size; i++)
    {
        _data[i] = rawArray[i];
    }
}

Vector::Vector(const Vector& other)
{
    _size = other.size();
    _capacity = other.size();
    _data = new Value[_capacity];
    _multiplicativeCoef = other._multiplicativeCoef;
    
    for (size_t i = 0; i < _capacity; i++)
    {
        _data[i] = other._data[i];
    }
}

Vector& Vector::operator=(const Vector& other)
{
    if (this == &other)
    {
        return *this;
    }

    delete[] _data;
	
    _multiplicativeCoef = other._multiplicativeCoef;
    _size = other._size;
    _capacity = other._size;
    _data = new Value[_size]
    
    for (size_t i = 0; i < _size; i++)
    {
        _data[i] = other._data[i];
    }
    
    return *this;
}

Vector::Vector(Vector&& other) noexcept
{
    _multiplicativeCoef = other._multiplicativeCoef;
    _data = other._data;
    _size = other._size;
    _capacity = other._capacity;
    
    other._multiplicativeCoef = 0;
    other._size = 0;
    other._data = nullptr;
    other._capacity = 0;
}

Vector& Vector::operator=(Vector&& other) noexcept
{
    if (this == &other)
    {
        return *this;
    }
    
    _multiplicativeCoef = other._multiplicativeCoef;
    _data = other._data;
    _size = other._size;
    _capacity = other._capacity;
    
    other._multiplicativeCoef = 0;
    other._size = 0;
    other._data = nullptr;
    other._capacity = 0;
    
    return *this;
}

void Vector::memory()
{
    _capacity = _size * _multiplicativeCoef;
    Value* tmp = new Value[_size];
        
    for (size_t i = 0; i < _size; i++)
    {
        tmp[i] = _data[i];
    }
    
    delete[] _data;
    _data = new Value[_capacity];
        
    for(size_t j = 0; j < _size; j++)
    {
        _data[j] = tmp[j];
    }
        
    delete[] tmp;
}	

void Vector::pushBack(const Value& value)
{
    _size += 1;
    
    if (_size > _capacity)
    {
        memory();
    }

    _data[_size - 1] = value;
}

void Vector::pushFront(const Value& value)
{
    _size += 1;
    
    if (_size  > _capacity)
    {
        memory();
    }
    
    for (size_t i = _size; i > 0 ; i--)
    {
        _data[i] = _data[i - 1];
    }
    
    _data[0] = value;
}

void Vector::insert(const Value& value, size_t pos)
{
    if (_size >= _capacity)
    {
        memory();
    }
    
    for (size_t i = _size; i > pos; i--)
    {
        _data[i] = _data[i - 1];
    }
    
    _data[pos] = value;
    _size++;
}

void Vector::insert(const Value* values, size_t size, size_t pos)
{
    size_t tSize = _size + size;
    
    while (tSize >= _capacity)
    {
        memory();
    }
    
    for (size_t i = _size - 1; i >= pos; i--)
    {
        _data[size + i] = _data[i];
    }
    
    for (size_t i = 0; i < size; i++)
    {
        _data[i + pos] = values[i];
    }
    
    _size += size;
}

void Vector::insert(const Vector& vector, size_t pos)
{
    insert(vector._data, vector._size, pos);
}

void Vector::popBack()
{
    if (_size <= 0)
    {
        throw std::invalid_argument("empty vector");
    }
    
    _size--;
}

void Vector::popFront()
{
    if (_size <= 0)
    {
        throw std::invalid_argument("empty vector");
    }
    
    for (size_t i = 0; i < _size - 1; i++)
    {
        _data[i] = _data[i + 1];
    }
    
    _size--;
}

void Vector::erase(size_t pos, size_t count)
{
    size_t e_count = std::min(count, _size - pos);
    
    for (size_t  i = pos; i < _size - e_count;  i++)
    {
        _data[i] = _data[e_count + i];
    }
    
    _size -= e_count;
}

void Vector::eraseBetween(size_t beginPos, size_t endPos)
{
    erase(beginPos, endPos - beginPos);
}

size_t Vector::size() const
{
    return _size;
}

size_t Vector::capacity() const
{
    return _capacity;
}

double Vector::loadFactor() const
{
    return (double(_size) / _capacity);
}

Value& Vector::operator[](size_t idx){
    return _data[idx];
}

long long Vector::find(const Value& value) const
{
    for (size_t i = 0; i < _size; i++)
    {
        if (_data[i] == value)
		{
            return i;
        }
    }
	
    return -1;
}

void Vector::reserve(size_t capacity)
{
    if (capacity > _capacity)
    {
        Value* tmp = new Value[capacity];
        
        for (size_t i = 0; i < _capacity; i++)
		{
            tmp[i] = _data[i];
        }
        
        delete[] _data;
        _capacity = capacity;
        _data = new Value[_capacity];
        
        for(size_t j = 0; j < _size; j++)
		{
            _data[j] = tmp[j];
        }
		
        delete[] tmp;
    }
    
    else
	{
        return;
    }
}

void Vector::shrinkToFit()
{
    _capacity = _size;
    Value* tmp = new Value[_size];
    
    for (size_t i = 0; i < _size; i++)
	{
        tmp[i] = _data[i];
    }
    
    delete[] _data;
    _data = tmp;
}

Vector::Iterator::Iterator(Value* ptr) : _ptr(ptr)
{
    
}

Value& Vector::Iterator::operator*()
{
    return *_ptr;
}

const Value& Vector::Iterator::operator*() const
{
    return *_ptr;
}

Value* Vector::Iterator::operator->()
{
    return _ptr;
}

Vector::Iterator Vector::Iterator::operator++()
{
    _ptr++;
    return *this;
}

Vector::Iterator Vector::Iterator::operator++(int)
{
    Vector::Iterator tmp(_ptr);
    _ptr++;
    return tmp;
}

const Value* Vector::Iterator::operator->() const
{
    return _ptr;
}

bool Vector::Iterator::operator==(const Vector::Iterator& other) const
{
    return _ptr == other._ptr;
}

bool Vector::Iterator::operator!=(const Vector::Iterator& other) const
{
    return _ptr != other._ptr;
}

Vector::Iterator Vector::begin()
{
    Vector::Iterator tmp(_data);
    return tmp;
}

Vector::Iterator Vector::end()
{
    Vector::Iterator tmp(_data + _size);
    return tmp;
}
