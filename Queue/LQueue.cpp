#include "LQueue.h"
#include <iostream>

void ListQueue::push(const ValueType& value) {
    _data.push_back(value);
}

void ListQueue::pop() {
    _data.pop_front();
}

const ValueType& ListQueue::front() const{
    return _data.front();
}

bool ListQueue::isEmpty() const {
    if (_data.size() != 0){
        return 1;
    }
    return 0;
}

size_t ListQueue::size() const {
    return _data.size();
}
