#include "Queue.h"
#include "LQueue.h"
#include "VQueue.h"
#include "QueueImplementation.h"
#include <iostream>
#include <stdexcept>

Queue::Queue(QueueContainer container) : _containerType(container)
{
    switch (container)
    {
        case QueueContainer::List:
            _pimpl = static_cast<IQueueImplementation*>(new ListQueue());
            break;
        case QueueContainer::Vector:
            _pimpl = static_cast<IQueueImplementation*>(new VectorQueue);
            break;
        default:
            throw std::runtime_error("Неизвестный тип контейнера");
    }
}

Queue::Queue(Queue&& moveQueue) noexcept :_containerType(moveQueue._containerType)
{
    switch (_containerType)
    {
        case QueueContainer::List:
            _pimpl = static_cast<IQueueImplementation*>(new ListQueue);
            break;
        case QueueContainer::Vector:
            _pimpl = static_cast<IQueueImplementation*>(new VectorQueue);
            break;
        default:
            throw std::runtime_error("Неизвестный тип контейнера");
    }
    
    _pimpl = moveQueue._pimpl;
    moveQueue._pimpl = nullptr;
}

Queue& Queue::operator=(Queue&& moveQueue) noexcept {
    if (this == &moveQueue) {
        return *this;
    }
    
    switch (moveQueue._containerType)
    {
        case QueueContainer::List:
            _pimpl = static_cast<IQueueImplementation*>(new ListQueue);
            break;
        case QueueContainer::Vector:
            _pimpl = static_cast<IQueueImplementation*>(new VectorQueue);
            break;
        default:
            throw std::runtime_error("Неизвестный тип контейнера");
    }
	
    _containerType = moveQueue._containerType;
    _pimpl = moveQueue._pimpl;
    moveQueue._pimpl = nullptr;
    return *this;
}

Queue::Queue(const ValueType* valueArray, const size_t arraySize, QueueContainer container) : _containerType(container)
{
    switch (container)
    {
        case QueueContainer::List:
            _pimpl = static_cast<IQueueImplementation*>(new ListQueue);
            break;
        case QueueContainer::Vector:
            _pimpl = static_cast<IQueueImplementation*>(new VectorQueue);
            break;
        default:
            throw std::runtime_error("Неизвестный тип контейнера");
    }
    
    for (int i = 0; i < arraySize; i++){
        _pimpl->push(valueArray[i]);
    }
}

Queue::Queue(Queue& copyQueue) : _containerType(copyQueue._containerType) {
    switch (_containerType)
    {
        case QueueContainer::List:
            _pimpl = static_cast<IQueueImplementation*>(new ListQueue);
            break;
        case QueueContainer::Vector:
            _pimpl = static_cast<IQueueImplementation*>(new VectorQueue);
            break;
        default:
            throw std::runtime_error("Неизвестный тип контейнера");
     }

    const int queueSize = copyQueue.size();
    std::vector<ValueType> tmp;

    for (int i = 0; i < queueSize; i++)
    {
        tmp.push_back(copyQueue._pimpl->front());
        copyQueue._pimpl->pop();
    }

    for (int j = 0; j < queueSize; j++)
    {
        push(tmp[j]);
        copyQueue._pimpl->push(tmp[j]);
    }
}

Queue& Queue::operator=(Queue& copyQueue) {
    if(&copyQueue == this){
        return *this;
    }
	
    delete _pimpl;
    _containerType=copyQueue._containerType;
    
    switch (_containerType)
    {
        case QueueContainer::List:
            _pimpl = static_cast<IQueueImplementation*>(new ListQueue());
            break;
        case QueueContainer::Vector:
            _pimpl = static_cast<IQueueImplementation*>(new VectorQueue);
            break;
        default:
            throw std::runtime_error("Неизвестный тип контейнера");
    }
    
    const int queueSize = copyQueue.size();
    std::vector<ValueType> tmp;

    for (int i = 0; i < queueSize; i++)
    {
        tmp.push_back(copyQueue._pimpl->front());
        copyQueue._pimpl->pop();
    }

    for (int j = 0; j < queueSize; j++)
    {
        push(tmp[j]);
        copyQueue._pimpl->push(tmp[j]);
    }
    
    return *this;
}

Queue::~Queue()
{
    delete _pimpl;
}

void Queue::push(const ValueType& value)
{
    _pimpl->push(value);
}

void Queue::pop()
{
    _pimpl->pop();
}

const ValueType& Queue::front() const
{
    return _pimpl->front();
}

bool Queue::isEmpty() const
{
    if (_pimpl->isEmpty() != 1){
        return 1;
    }
    return 0;
}

size_t Queue::size() const
{
    return _pimpl->size();
}


