#pragma once
#include <cstddef>

using ValueType = double;


enum class QueueContainer {
    Vector = 0,
    List,
};

class IQueueImplementation;

class Queue
{
public:
    Queue(QueueContainer container = QueueContainer::Vector);
    
    Queue(const ValueType* valueArray, const size_t arraySize, QueueContainer container = QueueContainer::Vector);

    explicit Queue(Queue& copyQueue);
    Queue& operator=(Queue& copyQueue);

    Queue(Queue&& moveQueue) noexcept;
    Queue& operator=(Queue&& moveQueue) noexcept;

    ~Queue();

    void push(const ValueType& value);
    void pop();
    const ValueType& front() const;
    bool isEmpty() const;
    size_t size() const;
private:
    
    IQueueImplementation* _pimpl = nullptr;
    QueueContainer _containerType;
};

