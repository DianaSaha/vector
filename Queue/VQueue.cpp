#include "VQueue.h"
#include <iostream>

void VectorQueue::push(const ValueType& value) {
    _data.push_back(value);
}

void VectorQueue::pop() {
    for (size_t i = 0; i < _data.size() - 1; i++){
        _data[i] = _data[i + 1];
    }
    
    _data.pop_back();
}

const ValueType& VectorQueue::front() const {
    return _data.front();
}

bool VectorQueue::isEmpty() const {
    if (_data.size() != 0){
        return 1;
    }
    return 0;
}

size_t VectorQueue::size() const {
    return _data.size();
}
