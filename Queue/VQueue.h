#pragma once
#include <vector>
#include "QueueImplementation.h"
#include <stdexcept>

using ValueType = double;

class VectorQueue : public IQueueImplementation {
private:
    std::vector <ValueType> _data;
public:
    VectorQueue() = default;
    
    ~VectorQueue() = default;
    
    void push(const ValueType& value) override;
    
    void pop() override;
    
    const ValueType& front() const override;
    
    bool isEmpty() const override;
    
    size_t size() const override;
};
