#pragma once
#include "QueueImplementation.h"
#include <list>
#include <stdexcept>

using ValueType = double;

class ListQueue : public IQueueImplementation {
private:
    
    std::list<ValueType> _data;
public:
    
    ListQueue() = default;
    
    ~ListQueue() = default;
    
    void push(const ValueType& value) override;
    
    void pop() override;
    
    const ValueType& front() const override;
    
    bool isEmpty() const override;
    
    size_t size() const override;
};
