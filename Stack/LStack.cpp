#include "LStack.h"
#include <iostream>

void ListStack::push(const ValueType& value) {
    _data.push_back(value);
}

void ListStack::pop() {
    _data.pop_back();
}

const ValueType& ListStack::top() const{
    return _data.back();
}

bool ListStack::isEmpty() const {
    if (_data.size() != 0){
        return 1;
    }
    return 0;
}

size_t ListStack::size() const {
    return _data.size();
}
