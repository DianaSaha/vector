#pragma once
#include <vector>
#include "StackImplementation.h"
#include <list>
#include <stdexcept>

using ValueType = double;

class VectorStack : public IStackImplementation {
private:
    
    std::vector <ValueType> _data;
public:
    
    VectorStack() = default;
    
    ~VectorStack() = default;
    
    void push(const ValueType& value) override;
    
    void pop() override;
    
    const ValueType& top() const override;
    
    bool isEmpty() const override;
    
    size_t size() const override;
};
