#include "Stack.h"
#include "VStack.h"
#include "LStack.h"
#include "StackImplementation.h"
#include <iostream>
#include <stdexcept>

Stack::Stack(StackContainer container) : _containerType(container)
{
    switch (container)
    {
        case StackContainer::List:
            _pimpl = static_cast<IStackImplementation*>(new ListStack());
            break;
        case StackContainer::Vector:
            _pimpl = static_cast<IStackImplementation*>(new VectorStack);
            break;
        default:
            throw std::runtime_error("Неизвестный тип контейнера");
    }
}

Stack::Stack(const ValueType* valueArray, const size_t arraySize, StackContainer container) : _containerType(container)
{
    switch (container)
    {
        case StackContainer::List:
            _pimpl = static_cast<IStackImplementation*>(new ListStack);
            break;
        case StackContainer::Vector:
            _pimpl = static_cast<IStackImplementation*>(new VectorStack);
            break;
        default:
            throw std::runtime_error("Неизвестный тип контейнера");
    }
    
    for (int i = 0; i < arraySize; i++){
        _pimpl->push(valueArray[i]);
    }
}

Stack::Stack(Stack& copyStack) : _containerType(copyStack._containerType) {
    switch (_containerType)
    {
        case StackContainer::List:
            _pimpl = static_cast<IStackImplementation*>(new ListStack);
            break;
        case StackContainer::Vector:
            _pimpl = static_cast<IStackImplementation*>(new VectorStack);
            break;
        default:
            throw std::runtime_error("Неизвестный тип контейнера");
    }
    
    if (&copyStack == this) {
        return;
    }
    
    *this = copyStack;
}

Stack& Stack::operator=(Stack& copyStack) {
    if(&copyStack == this){
        return *this;
    }
    
    delete _pimpl;
    _containerType=copyStack._containerType;
    
    switch (_containerType)
    {
        case StackContainer::List:
            _pimpl = static_cast<IStackImplementation*>(new ListStack());
            break;
        case StackContainer::Vector:
            _pimpl = static_cast<IStackImplementation*>(new VectorStack);
            break;
        default:
            throw std::runtime_error("Неизвестный тип контейнера");
    }
    
    const int stackSize = copyStack.size();
    std::vector<ValueType> tmp;
    
    for (size_t i = 0; i < stackSize; i++){
        tmp.push_back(copyStack._pimpl->top());
        copyStack._pimpl->pop();
    }
    
    for (size_t i = (stackSize - 1); i > 0; i--){
        push(tmp[i]);
        copyStack.push(tmp[i]);
    }
    
    push(tmp[0]);
    copyStack.push(tmp[0]);
    return *this;
}

Stack::Stack(Stack&& moveStack) noexcept :_containerType(moveStack._containerType)
{
    switch (_containerType)
    {
        case StackContainer::List:
            _pimpl = static_cast<IStackImplementation*>(new ListStack());
            break;
        case StackContainer::Vector:
            _pimpl = static_cast<IStackImplementation*>(new VectorStack);
            break;
        default:
            throw std::runtime_error("Неизвестный тип контейнера");
    }
    
    _pimpl = moveStack._pimpl;
    moveStack._pimpl = nullptr;
}

Stack& Stack::operator=(Stack&& moveStack) noexcept {
    if (this == &moveStack) {
        return *this;
    }
    
    switch (moveStack._containerType)
    {
        case StackContainer::List:
            _pimpl = static_cast<IStackImplementation*>(new ListStack());
            break;
        case StackContainer::Vector:
            _pimpl = static_cast<IStackImplementation*>(new VectorStack);
            break;
        default:
            throw std::runtime_error("Неизвестный тип контейнера");
    }
    _containerType = moveStack._containerType;
    _pimpl = moveStack._pimpl;
    moveStack._pimpl = nullptr;
    
    return *this;
}

Stack::~Stack()
{
    delete _pimpl;
}

void Stack::push(const ValueType& value)
{
    _pimpl->push(value);
}

void Stack::pop()
{
    _pimpl->pop();
}

const ValueType& Stack::top() const
{
    return _pimpl->top();
}

bool Stack::isEmpty() const
{
    if (_pimpl->isEmpty() != 1){
        return 1;
    }
    return 0;
}

size_t Stack::size() const
{
    return _pimpl->size();
}


