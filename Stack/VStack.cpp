#include "VStack.h"
#include <iostream>

void VectorStack::push(const ValueType& value) {
    _data.push_back(value);
}

void VectorStack::pop() {
    _data.pop_back();
}

const ValueType& VectorStack::top() const {
    return _data.back();
}

bool VectorStack::isEmpty() const {
    if (_data.size() != 0){
        return 1;
    }
    return 0;
}

size_t VectorStack::size() const {
    return _data.size();
}
