#pragma once

#include "StackImplementation.h"
#include <list>
#include <stdexcept>

using ValueType = double;

class ListStack : public IStackImplementation {
private:
    
     std::list<ValueType> _data;
public:
    
    ListStack() = default;
    
    ~ListStack() = default;
    
    void push(const ValueType& value) override;
    
    void pop() override;
    
    const ValueType& top() const override;
    
    bool isEmpty() const override;
    
    size_t size() const override;
};
